//
//  CameraEngline.swift
//  CameraApp
//
//  Created by Alexander B on 06/04/2019.
//  Copyright © 2019 Alexander B. All rights reserved.
//

import AVFoundation

//
//  ViewController.swift
//  movieTest
//
//  Created by Alexander B on 02/08/2018.
//  Copyright © 2018 Alexander B. All rights reserved.
//
/*
  
 TODO:
 Fix when someone is calling you
 Fix when listening to music (add mute recording with a user warning) etc

 
 Fix animation when charger is plugged in ???
 
 Fix merger audio (add sample buffer ramps to the start / end of each track
 Fix blank recording at the start


 ERRORS:
 (Status 3 when no frames in the buffer. workaround: check if there is a frame present) mostly fixed <----
 
 */



/*

 Recording
 Storage
 Asset Manipulation
 
 */

class CameraEngineStorage: NSObject {
    
    
    
}

@objc protocol EngineDelegate: class {
//    var isRecording: Bool { get set }
    @objc optional func startedRecording()
    @objc optional func endedRecording()
    @objc optional var timer: CGFloat { get set }
}

protocol EngineProtocol {
    var isRecording: Bool { get set }

}


class CameraEngine: NSObject, AVCaptureVideoDataOutputSampleBufferDelegate, AVCaptureAudioDataOutputSampleBufferDelegate, EngineProtocol {
    

    weak var delegate: EngineDelegate?
    static var sharedInstance = CameraEngine()
    
    var locked = false
    
    var zoomFactor: CGFloat = 0.0 {
        didSet {
            
            locked = true
            let currentCameraDevice = activeVideoInput[0].device
            let videoMaxZoomFactor = currentCameraDevice.activeFormat.videoMaxZoomFactor
            
            let constrainedZoomLevel = min(max(1.0, zoomFactor + 1.0), videoMaxZoomFactor)
            print(constrainedZoomLevel)

            

            do {
                try currentCameraDevice.lockForConfiguration()
                currentCameraDevice.videoZoomFactor = constrainedZoomLevel
                currentCameraDevice.unlockForConfiguration()
            } catch {
                print(error)
            }
            
            
            
        }
    }
    
    
    var session = AVCaptureSession()

    //var frameCounter
    
    let isAutomated = false
    var isSwitchingFace = false
    
    
    // is recording video is there to make sure there is a frame of recording then stops synced to user input
    var isRecordingVideo = false {
        didSet {
            //transitionButton.isHidden = isRecordingVideo
            
        }
    }
    
    // is recording is responsible for start and stop recording flows with buffer, something like this
    var isRecording = false {
        willSet {
            if newValue == isRecording { return }
            if newValue == true {
//                delegate?.isRecording = true
                CameraEngine.sharedInstance.delegate?.startedRecording?() ?? print("startedRecording DELEGATE NOT SET")
            } else {
//                delegate?.isRecording = false
                CameraEngine.sharedInstance.delegate?.endedRecording?() ?? print("endedRecording DELEGATE NOT SET")
            }
        }
    }

    

    
    //var audioSession = AVAudioSession()
    //preview - manage the preview layer
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    var frameCounter = 0 {
        didSet {
            print(frameCounter)
        }
    }
    //max level of zoom is 10
    
    let serialQueue = DispatchQueue(label: "SerialQueue")
    
    var writerStatusObserver: Any?
    
    var outputFileLocation: URL!
    
    var sessionAtSourceTime: CMTime!
    var endSessionTime: CMTime!
    
    var writer: AVAssetWriter!
    
    
    var audioDataOutput = AVCaptureAudioDataOutput()
    var videoDataOutput = AVCaptureVideoDataOutput()
    
    var audioWriterInput: AVAssetWriterInput!
    var videoWriterInput: AVAssetWriterInput!
    
    @objc dynamic var assetArray = [AVAsset]() {
        didSet {
            if assetArray.count > 0 {
                DispatchQueue.main.async {

                }
            }
        }
    }
    
    
    var activeInputs: [AVCaptureDeviceInput] {
        return session.inputs as! [AVCaptureDeviceInput]
    }
    
    
    var activeVideoInput: [AVCaptureDeviceInput] {
        //need device availability check on this at some point
        var devicesToReturn =  [AVCaptureDeviceInput]()
        let _ = activeInputs.compactMap { if $0.description.contains("Camera") { devicesToReturn.append($0) }
        }
        return devicesToReturn
    }
    
    
    
    
    func getVideoDeviceBasedOnPosition(position: AVCaptureDevice.Position) -> AVCaptureDevice? {
        //TODO:Edit to work every camera type
        let devices = AVCaptureDevice.DiscoverySession(deviceTypes: [ .builtInWideAngleCamera, .builtInDualCamera, .builtInTelephotoCamera], mediaType: AVMediaType.video, position: .unspecified).devices
        
        for device in devices {
            if device.position == position {
                return device
            }
        }
        return nil
    }
    
    //    func getAllAudioDevices() -> [AVCaptureDevice]
    
    
    
  
    
    func rotateCameraInput() {
        DispatchQueue.main.async {
            
            self.isSwitchingFace = true
            

            //                        self.transitionButton.isHidden = false
            //                    }
       
        
            if self.writer != nil {
                
            }
            
            var deviceToChangeToInTheFuture: AVCaptureDevice?
            var futureVideoInput: AVCaptureDeviceInput?
            
            let currentInput = self.activeVideoInput[0]  // ????
            
            defer {
                
                self.fixVideoOrientation()
                if currentInput.device.position == .back {
                    self.fixMirroring()
                }
                
                //
                if self.isRecordingVideo {
                    self.start()

                }
                
                self.isSwitchingFace = false
                
            }
            
            guard self.activeVideoInput.count != 0 else { return }
            
            
            if currentInput.device.position == .back {
                deviceToChangeToInTheFuture = self.getVideoDeviceBasedOnPosition(position: .front)
                
            } else {
                deviceToChangeToInTheFuture = self.getVideoDeviceBasedOnPosition(position: .back)
            }
            
            guard let unwrappedDeviceToChangeToInTheFuture = deviceToChangeToInTheFuture else {
                print("problem with face changing method"); return }
            
            do {
                try futureVideoInput = AVCaptureDeviceInput(device: unwrappedDeviceToChangeToInTheFuture) }
            catch let error {
                print("error adding camera in changeFace with error \(error.localizedDescription)")
            }
            self.finishWriter {}
            

            
            //_ = activeInputs.compactMap({ session.removeInput($0) })
            self.session.beginConfiguration()
            self.session.removeInput(currentInput)
            self.session.addInput(futureVideoInput!)
            self.session.commitConfiguration()
            
            
            
        }
    }

    
    func fixVideoOrientation() {
        //change to observer of the connection
        for connection in videoDataOutput.connections {
            for port in connection.inputPorts {
                if port.mediaType == .video {
                    connection.videoOrientation = .portrait
                }
            }
        }
    }
    
    func mirrorVideo() {
        for connection in videoDataOutput.connections {
            for port in connection.inputPorts {
                if port.mediaType == .video {
                    if connection.isVideoMirrored == false {
                        connection.isVideoMirrored = true
                    } else {
                        connection.isVideoMirrored = false
                    }
                }
            }
        }
    }
    
    
    func fixMirroring() {
        for connection in videoDataOutput.connections {
            for port in connection.inputPorts {
                if port.mediaType == .video {
                    if connection.isVideoMirrored == false {
                        connection.isVideoMirrored = true
                    }
                }
            }
        }
    }
    
    enum Status {
        case granted
        case denied
    }
    
    func startSession() {
        
        AVCaptureDevice.requestAccess(for: AVMediaType.video) { response in
            if response {
                //access granted
            } else {
                
            }
        }

        
        
        
        AVCaptureDevice.requestAccess(for: AVMediaType.audio) { response in
            if response {
                //access granted
            } else {
                
            }
        }
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: session)
        videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        
        do {
            let input = try AVCaptureDeviceInput(device: getVideoDeviceBasedOnPosition(position: AVCaptureDevice.Position.back)!)
            if session.canAddInput(input) {
                session.addInput(input)
                

                
            }
        } catch {
            print("Error setting device video input: \(error)")
        }
        
        
        if let defaultAudioDevice = AVCaptureDevice.default(.builtInMicrophone, for: AVMediaType.audio, position: .unspecified) {
            
            do {
                
                let input = try AVCaptureDeviceInput(device: defaultAudioDevice)
                if session.canAddInput(input) {
                    session.addInput(input)
                }
            } catch {
                print("Error setting device audio input: \(error)")
            }
        }
        
        
        if session.canAddOutput(audioDataOutput) {
            session.addOutput(audioDataOutput)
        }
        
        if session.canAddOutput(videoDataOutput) {
            session.addOutput(videoDataOutput)
        }
        
        
        self.session.startRunning()
        
        
        //Good example of configuration locking
        //check here if the view is correct for rendering

        
    }


    var generatedURL: URL? {
        let directory = NSTemporaryDirectory() as NSString
        if directory != "" {
            let path = directory.appendingPathComponent(NSUUID().uuidString + ".mp4")
            return URL(fileURLWithPath: path)
        }
        return nil
    }

        

    func setUpWriter() {
        //writer.status
        if writer != nil { return }
        
        videoDataOutput.setSampleBufferDelegate(self, queue: serialQueue)
        audioDataOutput.setSampleBufferDelegate(self, queue: serialQueue)

        do {
            outputFileLocation = generatedURL
            
            writer = try AVAssetWriter(outputURL: outputFileLocation!, fileType: AVFileType.mov)
            writerStatusObserver = writer.observe(\.status, options: [.new]) { writer, _ in print("STATUS IS NOW \(writer.status.rawValue)");
                switch writer.status {
                case .completed: break
                    //                    DispatchQueue.main.async {
                    //                        self.transitionButton.isHidden = false
                //                    }
                case .failed:
                    DispatchQueue.main.async {
                        debugPrint(writer.error!)
                        debugPrint(writer.error!.localizedDescription)
                        //assertionFailure("STATUS 3 OF THE WRITER IS REACHED")
                        print("status 3 is reached")
                        
                    }
                    
                default:
                    break
                }
            }
            
            let videoFrameWidth = 750.0
            let videoFrameHeight = 1624.0
            
            let numPixels: CGFloat = CGFloat(videoFrameWidth * videoFrameHeight)
            let bitsPerPixel: CGFloat = 11.4
            let bitsPerSecond = numPixels * bitsPerPixel
            
            // add video input
            videoWriterInput = AVAssetWriterInput(mediaType: AVMediaType.video, outputSettings: [
                AVVideoScalingModeKey : AVVideoScalingModeResizeAspectFill,
                AVVideoCodecKey : AVVideoCodecType.h264,
                AVVideoWidthKey : videoFrameWidth,
                AVVideoHeightKey : videoFrameHeight,
                AVVideoCompressionPropertiesKey : [
                    AVVideoAverageBitRateKey : bitsPerSecond,
                ],
                ])
            
            session.sessionPreset = .high
            videoWriterInput.expectsMediaDataInRealTime = true
            if writer.canAdd(videoWriterInput) {
                writer.add(videoWriterInput)
                
                
                print("video input added")
            } else {
                print("no input added")
            }
            
            // add audio input
            audioWriterInput = AVAssetWriterInput(mediaType: AVMediaType.audio, outputSettings: nil)
            audioWriterInput.expectsMediaDataInRealTime = true
            
            if writer.canAdd(audioWriterInput!) {
                writer.add(audioWriterInput!)
                print("audio input added")
            }
            
        } catch let error {
            debugPrint(error.localizedDescription)
        }
      
        
        
    }
    
    func canWrite() -> Bool {
        return writer != nil && writer?.status == .writing
    }
    
    
    
    
    // MARK: AVCaptureVideoDataOutputSampleBufferDelegate
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        
        //STATES: writable -> writer != nil, writer.status == .writing, isRecordingVideo == true
        //
        //
        //
        
        
        

        
        DispatchQueue.main.async {
            //add timer count += when automation is on
            
            
            
            //
            if output == self.videoDataOutput {
                self.endSessionTime = CMSampleBufferGetOutputPresentationTimeStamp(sampleBuffer)
                
            }
            
            //((PresentationTimeStamp + TrimDurationAtStart - EditStartMediaTime) / EditSpeedMultiplier) + EditStartTrackTime
            
            
            
            
            let writable = self.canWrite()
            
            var inputsReady = false
            if self.videoWriterInput.isReadyForMoreMediaData && self.audioWriterInput.isReadyForMoreMediaData {
                inputsReady = true
            } else { inputsReady = false }
            
            if writable,
                self.sessionAtSourceTime == nil, output == self.videoDataOutput {
                // start writing
                self.sessionAtSourceTime = CMSampleBufferGetOutputPresentationTimeStamp(sampleBuffer)
                
                self.writer.startSession(atSourceTime: self.sessionAtSourceTime!)
            }
            
            if writable, inputsReady, output == self.videoDataOutput//, !self.isSwitchingFace
            {
                
                // write video buffer
                self.videoWriterInput.append(sampleBuffer)
                self.isRecording = true
                
                print("BUFFERING VIDEO")
               
                self.frameCounter += 1

                if !self.isRecordingVideo && self.frameCounter > 1 {
                    
                    self.frameCounter = 0
                    self.finishWriter(completion: {})
                    
                }
                
                
            }
            
            
            if writable, inputsReady, output == self.audioDataOutput//, !self.isSwitchingFace
            {
                // write audio buffer
                self.audioWriterInput.append(sampleBuffer)
                self.isRecording = true
//                if !self.isRecordingVideo {
//
//                    self.stop {
//
//                    }
//
//                }
            }
        }
        
    }
    // TODO: add asset duration meter to find out the issue with blank screen
    
    
    // MARK: Start recording
    func start() {
        DispatchQueue.main.async {

        if self.writer?.status.rawValue != 1 && !self.isSwitchingFace {
            if self.writer?.status.rawValue != 2 {
                    //            if self.writer?.status.rawValue != 1 || self.writer?.status.rawValue != 2 {
                    self.setUpWriter()
                    self.sessionAtSourceTime = nil
                    self.writer.startWriting()
                    
                }
            }
            //
            
//        }
            
            
        }
        
        
        
        isRecordingVideo = true
        fixVideoOrientation()

    }
    
    
    // MARK: Stop recording
    func stop(completion: @escaping () -> ()) {

        isRecordingVideo = false

    }
    func finishWriter(completion: @escaping () -> ()) {
        // guard let _: Bool? = writer?.status.rawValue != 1 else { return }
        if isRecording {
            //        if writer?.status.rawValue != 1 {
            videoWriterInput.markAsFinished()
            audioWriterInput.markAsFinished()
            
            print("marked as finished")
            writer.endSession(atSourceTime: endSessionTime)
            // writer.endSession(atSourceTime: CMTimeAdd(sessionAtSourceTime, endSessionTime))
            
            writer.finishWriting { [weak self] in
                self?.writer = nil
                self?.sessionAtSourceTime = nil
                self?.writerStatusObserver = nil
                if self?.outputFileLocation != nil {
                    self?.assetArray.append(AVAsset(url: (self?.outputFileLocation)!))
                }
                completion()
            }

            isRecording = false
        }
    }
    
    
    
    func mergedPlayerAssets() -> AVAsset? {
        if assetArray.count > 0 {
            
            return assetArray.merge()
        } else {
            return nil
        }
    }
    
}

extension Collection where Element == AVAsset, Index == Int  {
    
    //Fixing merger to have audio
    func merge() -> AVComposition? {
        let mixComposition = AVMutableComposition()
        
        var currentDuration = CMTime.zero
        if self.count == 0 {
            return nil
        }
        
        guard let track = mixComposition.addMutableTrack(withMediaType: .video,
                                                         preferredTrackID: Int32(kCMPersistentTrackID_Invalid)) else { return nil }
        
        guard let audioTrack = mixComposition.addMutableTrack(withMediaType: .audio,
                                                              preferredTrackID: Int32(kCMPersistentTrackID_Invalid)) else { return nil }
        
        
        
        for index in 0..<self.count {
            
            let asset = self[index]
            
            if asset.tracks(withMediaType: AVMediaType.video).count != 0 && asset.tracks(withMediaType: AVMediaType.audio).count != 0 {
                
                
                print(asset.tracks(withMediaType: AVMediaType.video)[0])
                let assetVideoTrack = asset.tracks(withMediaType: AVMediaType.video)[0]
                let assetAudioTrack = asset.tracks(withMediaType: AVMediaType.audio)[0]
                
                print(currentDuration.seconds)
                print(currentDuration.timescale)
                print(asset)
                //let adjustedDuration = CMTimeSubtract(asset.duration, CMTimeMake(value: 1,timescale: 10))
                
                do {
                    
                    
                    try track.insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: asset.duration),
                                              of: assetVideoTrack,
                                              at: currentDuration)
                    
                    try audioTrack.insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: asset.duration),
                                                   of: assetAudioTrack,
                                                   at: currentDuration)
                    
                    currentDuration = CMTimeAdd(currentDuration, asset.duration)
                    track.preferredTransform = assetVideoTrack.preferredTransform
                    
                } catch {
                    print("Failed to load first track")
                }
            }
            
        }
        
        
        return mixComposition as AVComposition
    }
    
    
}
