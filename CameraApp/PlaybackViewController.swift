//
//  PlaybackViewController.swift
//  AppApp
//
//  Created by Alexander B on 09/07/2018.
//  Copyright © 2018 Alexander B. All rights reserved.


import UIKit
import AVFoundation
import Photos
import CircleMenu

//:Issue: performance on playback

//Matrix of Assets
//Asset corresponds to Unit
//
//unit giphy
//unit text
//unit music
//            [+]
//|> [______] \|/
//        /|\
enum UIAsset {
    
}

class Modules {
    var assets = ["text": "add text"//,
        //                  "music": "add music",
        //                  "giphy": "add giphy",
        //                  "drawing": "add drawing"
    ]
}


//var assetCollection = [UIElements: () -> ()]
//


enum UIStateControl {
    
    case normalState
    case touched(asset: Asset)
    case moved(asset: Asset)
    
}



//add units to timeline
class Timeline {
    
    //var times = [(Double...)]
    //
    
    
}
//////////////////////////////////////////////////////////////
//asset has a delegate that fires back when file was edited
//timeline [0 - 1], [0 - video time should be synced up]
//check if its in range
//timeline in sync with asset ranges
//////////////////////////////////////////////////////////////


//[asset: func]

//sort hashable out
//var assetMethodCollection: [Asset: ()->()] = []


class PlaybackViewController: UIViewController, UITextFieldDelegate {
    
    func textTriggered(){
        for ui in UIs {
             
            ui.isHidden = true
            //textPreviewView.becomeFirstResponder()
            
        }
    }
    
    let engine = CameraEngine.sharedInstance
    
    
    var timeline: UIView = UIView()
    var UIs: [UIControl] = [] {
        
        didSet {
            for v in view.subviews {
                v.removeFromSuperview()
            }
            
            for v in UIs {
                view.addSubview(v)
            }
        }
    }
    
    
    var textViewThatsBeingEdited: TextUnit!
    
    var videoSize: CGSize = CGSize.zero
    
    
    
    let avPlayer = AVPlayer()
    var avPlayerLayer: AVPlayerLayer!
    
    
    var state: UIStateControl = .normalState {
        didSet {
            
            switch state {
                
            case .normalState:
                
                for ui in UIs {
                    ui.isHidden = false
                }
                
                break
                
                
            case let .touched(_ /*unit*/):
                
                //                //texts handling TEXT UIVIEWs HERE
                //
                //                if let textUnit = unit as? TextUnit {
                //                    for subview in view.subviews {
                //                        subview.isHidden = false
                //                    }
                //
                //
                //                    let oldPosition = textUnit.frame
                //                    textUnit.frame = CGRect(x: 0.0, y: 300.0, width: textUnit.bounds.width, height: textUnit.bounds.height)
                //                    //textUnit
                //
                //
                //                    let textField = UITextField()
                //
                //
                //
                //                    textPreviewView.delegate = self
                //
                //                    textViewThatsBeingEdited = textUnit
                //                    textPreviewView.addTarget(self, action: #selector(returnPressed), for: UIControl.Event.primaryActionTriggered)
                //
                //
                //                    //make text preview a text view !
                //
                //                    textPreviewView.text = textUnit.textLayer.string as! String?
                //                    print(textUnit.text); print(textUnit.font)
                //                    print("HERE")
                //                    textPreviewView.font = textUnit.textLayer.font as? UIFont
                //                    //        textPreviewView.minimumFontSize = textUnit.textLayer.fontSize
                //                    //UIFont(name: textUnit.textLayer.font, size: textUnit.textLayer.fontSize)// = textUnit.textLayer.font
                //                    textPreviewView.frame = textUnit.frame
                //                    print(textUnit.frame.width)
                //                    textPreviewView.textColor = UIColor(cgColor: textUnit.textLayer.foregroundColor!)
                //                    textPreviewView.returnKeyType = UIReturnKeyType.done
                //
                //                    //textPreviewView.return
                //
                //                    //textPreviewView.sizeToFit()
                //                    //get keyboard out
                //                    view.addSubview(textPreviewView)
                //
                //                    textPreviewView.becomeFirstResponder()
                //                }
                //
                
                
                break
            case .moved(_):
                break
            }
        }
    }
    
    
    
    lazy var label: UILabel = {
        let label = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        
        return label
    }()
    
    
    lazy var returnButton: UIButton = {
        let size: CGFloat = 50.0
        let origin = CGPoint(x: view!.bounds.minX + (size / 2.0), y: view!.bounds.minY + 50.0)
        
        let button = UIButton(frame: CGRect(origin: origin, size: CGSize(width: size, height: size)))
        let newColor = UIColor(ciColor: .red)
        button.tintColor = newColor
        button.setTitle("╳", for: .normal)
        button.addTarget(self, action: #selector(returnToRecording), for: UIControl.Event.touchUpInside)
        return button
    }()
    
    lazy var timelineView: TimelineView = {
        var mainFrame = TimelineView(frame: CGRect(x: view.bounds.width, y: view.bounds.maxY, width: 300.0, height: 60.0), cornerRadius: 0.03, gradients: [.orange, .orange])
        return mainFrame
    }()
    
    
    var isDone = false {
        didSet {
            if isDone {
                DispatchQueue.main.async {
                    self.saveButton.setTitle("Saved", for: .normal)
                }
            }
        }
    }
    
    lazy var textButton: UIButton = {
        let button = UIButton(frame: CGRect(x: view!.bounds.maxX - 160.0, y: view.bounds.maxY - 175.0, width: 160.0, height: 90.0))
        button.setTitle("T", for: .normal)
        
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        button.addTarget(self, action: #selector(addText), for: .touchDown)
        return button
    }()
    
    lazy var saveButton: UIButton = {
        let button = UIButton(frame: CGRect(x: view!.bounds.maxX - 160.0, y: view.bounds.maxY - 120.0, width: 160.0, height: 90.0))
        button.setTitle("↓", for: .normal)
        
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        button.addTarget(self, action: #selector(saveRecording), for: .touchDown)
        return button
    }()
    
    lazy var resetRecordingButton: UIButton = {
        let button = UIButton(frame: CGRect(x: view!.bounds.minX + 16.0, y: view.bounds.maxY - 120.0, width: 160.0, height: 90.0))
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        
        button.setTitle("↻", for: .normal)
        button.addTarget(self, action: #selector(unwindClearData), for: .touchDown)
        return button
    }()
    
    
    var isPlaying = true
    
    //optimise at a later opportunity
    var userAddedAssets = [TextUnit]() {
        didSet {
            for subv in view.subviews {
                if let sv = subv as? TextUnit {
                    sv.removeFromSuperview()
                }
            }
            
            for asset in userAddedAssets {
                view.addSubview(asset)
            }
        }
    }
    
    //lazy var addButton = {}()
 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //NotificationCenter.default
        isPlaying = true
        avPlayerLayer = AVPlayerLayer(player: avPlayer)
        avPlayerLayer.frame = view.bounds
        avPlayerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        view.layer.insertSublayer(avPlayerLayer, at: 0)
        view.layoutIfNeeded()
        
        //videoSize = engine.mergedPlayerAssets()!.tracks(withMediaType: AVMediaType.video)[0].naturalSize
        
        //        guard let windowRect = view.window?.bounds
        //        let windowWidth = windowRect?.width
        //        let ratio = windowWidth / 375.0
        
        
        
        
        //        let giphy = Bundle.main.url(forResource: "giphy", withExtension: "mp4")
        guard let giphy = Bundle.main.url(forResource: "giphy", withExtension: "mp4") else {
            // handle file not found here
            return
        }
        //        let playerItem = AVPlayerItem(url: giphy)
        
        let playerItem = AVPlayerItem(asset: engine.mergedPlayerAssets()!)
        avPlayer.replaceCurrentItem(with: playerItem)
        avPlayer.play()
        addVideoDidEndNotificationObserver()
        
        
        timelineView.center = CGPoint(x: view.bounds.midX, y: view.bounds.midY / 1.5) //???
        //        timelineView. set rect may be
        timelineView.isHidden = true
        addSubviews()
        
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        //        saveButton.translatesAutoresizingMaskIntoConstraints = false
        
    }
    
    
    
    func addSubviews() {
        
        UIs.append(returnButton)
        UIs.append(saveButton)
        UIs.append(resetRecordingButton)
        UIs.append(timelineView)
//        UIs.append(textButton)
        
        
        let button = CircleMenu(
            frame: CGRect(x: 200, y: 200, width: 50, height: 50),
            normalIcon:"icon_menu",
            selectedIcon:"icon_close",
            buttonsCount: 4,
            duration: 4,
            distance: 120)
        button.delegate = self
        button.layer.cornerRadius = button.frame.size.width / 2.0
        view.addSubview(button)
        
        
        //        timelineView.addConstraints()
        
        
    }
    
    @objc func addText(sender: UIButton!, placement: CGRect) {
        
        // Watermark Effect
        let size = UIScreen.main.bounds
        let scale = UIScreen.main.scale
        //        let imglogo = UIImage(named: "image.png")
        //        let imglayer = CALayer()
        //        imglayer.contents = imglogo?.cgImage
        //        imglayer.frame = CGRect(x: 5, y: 5, width: 100, height: 100)
        //        imglayer.opacity = 0.6
        
        
        // create text Layer
        let titleView = TextUnit(frame: CGRect(x: 0, y: 400.0, width: size.width, height: size.height / 10))
//        titleView
        
        
        //        userAddedAssets.append(titleView)
        //
        //
                //editTriggered(textUnit: titleView)
        //
        
        // state = .touched(asset: titleView)
        
        
        
        
        
    }
    
    
    //TO LOOK AT INTF
    //    textField.returnKeyType = UIReturnKeyType.Done
    //    Add this anywhere you like.
    //
    //    extension UITextView: UITextViewDelegate {
    //        public func textViewDidChange(_ textView: UITextView) {
    //            if text.last == "\n" { //Check if last char is newline
    //                text.removeLast() //Remove newline
    //                textView.resignFirstResponder() //Dismiss keyboard
    //            }
    //        }
    //    }
    
    
    @objc func returnPressed(textField: UITextField) {
        textField.resignFirstResponder()
        for asset in userAddedAssets {
            if asset == textViewThatsBeingEdited {
                asset.textLayer.string = textField.text
            }
        }
        
        textField.removeFromSuperview()
        
        for subview in view.subviews {
            subview.isHidden = false
            
            
        }
        
        textViewThatsBeingEdited = nil
        
    }
    
    func editTriggered(textUnit: TextUnit) -> TextUnit {
        for subview in view.subviews {
            subview.isHidden = true
        }
        
        
        let oldPosition = textUnit.frame
        textUnit.frame = CGRect(x: 0.0, y: 300.0, width: textUnit.bounds.width, height: textUnit.bounds.height)
        //textUnit
        
        
        let textPreviewView = UITextField()
        textPreviewView.delegate = self
        
        
        
        textViewThatsBeingEdited = textUnit
        textPreviewView.addTarget(self, action: #selector(returnPressed), for: UIControl.Event.primaryActionTriggered)
        
        textPreviewView.layer.addSublayer(textUnit.textLayer)
        //make text preview a text view !
        //
        //        textPreviewView.text = textUnit.textLayer.string as! String?
        //        print(textUnit.text); print(textUnit.font)
        //        print("HERE")
        //        textPreviewView.font = textUnit.textLayer.font as? UIFont
        //        //        textPreviewView.minimumFontSize = textUnit.textLayer.fontSize
        //        //UIFont(name: textUnit.textLayer.font, size: textUnit.textLayer.fontSize)// = textUnit.textLayer.font
        //        textPreviewView.frame = textUnit.frame
        //        print(textUnit.frame.width)
        //        textPreviewView.textColor = UIColor(cgColor: textUnit.textLayer.foregroundColor!)
        textPreviewView.returnKeyType = UIReturnKeyType.done
        
        //textPreviewView.return
        
        //textPreviewView.sizeToFit()
        //get keyboard out
        view.addSubview(textPreviewView)
        
        //textUnit.textLayer
        textPreviewView.becomeFirstResponder()
        //move text to height match
        
        
        //textUnit.isHidden = false
        
        
        
        // slide up the keyboard
        // get its height
        // move text based on height
        // set user interaction to false
        // make textview static visually
        //
        
        return TextUnit()
    }
    
    //    func doneEditingText() {
    //        for subview in view.subviews {
    //            subview.isHidden = false
    //        }
    //
    //    }
    
//    @IBAction func unwindClearDataRecordingVC(segue: UIStoryboardSegue) {
//        if let vc = segue.destination as? RecordingViewController {
//            vc.timerLaber.text = " ●"
//        }
//    }
    

    
    @objc func unwindClearData(segue: UIStoryboardSegue) {
        // self.unwind//(for: , towards: RecordingViewController)
//
//
//        if let vc = segue.destination as? RecordingViewController {
//            vc.timerLaber.text = " ●"
//        }
        
        self.performSegue(withIdentifier: "unwindClearData", sender: nil)

    }
    
    @objc func saveRecording(sender: UIButton!) {
        //save recording
        
        let outputURL = engine.generatedURL
        let asset = engine.mergedPlayerAssets()!
        let composition = AVMutableComposition()
        
        // get video track
        let vtrack = asset.tracks(withMediaType: AVMediaType.video)
        let videoTrack: AVAssetTrack = vtrack[0]
        //let vid_duration = videoTrack.timeRange.duration
        let vid_timerange = CMTimeRangeMake(start: CMTime.zero, duration: asset.duration)
        
        let videoSize = videoTrack.naturalSize
        
        
        let compositionvideoTrack: AVMutableCompositionTrack = composition.addMutableTrack(withMediaType: AVMediaType.video, preferredTrackID: CMPersistentTrackID())!
        do { try compositionvideoTrack.insertTimeRange(vid_timerange, of: videoTrack, at: CMTime.zero) }
        catch let error {
            print(error)
        }
        
        
        //        let titleLayer = CATextLayer()
        //        titleLayer.backgroundColor = UIColor.whiteColor().CGColor
        //        titleLayer.string = "Dummy text"
        //        titleLayer.font = UIFont(name: "Helvetica", size: 28)
        //        titleLayer.shadowOpacity = 0.5
        //        titleLayer.alignmentMode = kCAAlignmentCenter
        
        let videolayer = CALayer()
        videolayer.frame = CGRect(x: 0, y: 0, width: videoSize.width, height: videoSize.height)
        
        let parentlayer = CALayer()
        parentlayer.frame = CGRect(x: 0, y: 0, width: videoSize.width, height: videoSize.height)
        parentlayer.addSublayer(videolayer)
        print(videoSize.width)
        print(videoSize.height)
        if userAddedAssets.count > 0 {
            
            _ = userAddedAssets.compactMap { textUnit in
                print(textUnit.frame.width)
                print(textUnit.frame.height)
                
                
                
                let widthRatio = videoSize.width / UIScreen.main.bounds.width
                let heightRatio = videoSize.height / UIScreen.main.bounds.height
                
                //            let textLayer = CATextLayer()
                //
                //            textLayer.frame = CGRect(x: 0, y: 0, width: videoSize.width, height: videoSize.height)
                //textLayer.addSublayer(textUnit.textLayer)
                
                //            let copyOfLayer = textUnit.textLayer
                //            let oldFrameRect = textUnit.frame
                
                var newLayer = CATextLayer()
                
                for subview in view.subviews {
                    if subview == textUnit {
                        
                        let subframe = textUnit.frame
                        print(subframe.minX, subframe.minY, subframe.width, subframe.height)
                        
                        newLayer = textUnit.textLayer
                        
                        
                        
                        //when too lazy to rotate..
                        newLayer.frame = CGRect(x: subframe.minX * widthRatio, y: (videoSize.height - (subframe.maxY * heightRatio)), width: subframe.width * widthRatio, height: subframe.height * heightRatio)
                        newLayer.fontSize = newLayer.fontSize * heightRatio
                        
                    }
                }
                //let oldFramePoint = screenAssets[0].
                //let
                
                
                //getting frame
                //            print(newLayer.frame.minX, newLayer.frame.minY, newLayer.frame.width, newLayer.frame.height)
                print(widthRatio, heightRatio)
                //            screenAssets[0].layer.frame = CGRect(x: 0, y: 0, width: videoSize.width, height: videoSize.height)
                
                parentlayer.addSublayer(newLayer)
                
            }
        }
        
        
        let layerComposition = AVMutableVideoComposition()
        layerComposition.frameDuration = CMTimeMake(value: 1, timescale: 30)
        layerComposition.renderSize = videoSize
        layerComposition.animationTool = AVVideoCompositionCoreAnimationTool(postProcessingAsVideoLayer: videolayer, in: parentlayer)
        
        
        
        compositionvideoTrack.preferredTransform = videoTrack.preferredTransform
        
        let instruction = AVMutableVideoCompositionInstruction()
        instruction.timeRange = CMTimeRangeMake(start: CMTime.zero, duration: composition.duration)
        let videotrack = composition.tracks(withMediaType: AVMediaType.video)[0] as AVAssetTrack
        let layerinstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: videotrack)
        instruction.layerInstructions = NSArray(object: layerinstruction) as [AnyObject] as! [AVVideoCompositionLayerInstruction]
        layerComposition.instructions = NSArray(object: instruction) as! [AVVideoCompositionInstructionProtocol]
        
        guard let exportSession = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetHighestQuality) else {
            return
        }
        
        exportSession.outputFileType = AVFileType.mov
        exportSession.outputURL = outputURL
        saveButton.setTitle("O", for: .normal)
        saveButton.isUserInteractionEnabled = false
        
        
        exportSession.videoComposition = layerComposition
        
        
        exportSession.exportAsynchronously {
            
            
            
            guard let url = outputURL else { return }
            PHPhotoLibrary.shared().performChanges({
                PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: url)
            }) { saved, error in
                if saved {
                    
                    self.isDone = true
                    
                    //                    let alertController = UIAlertController(title: "Your video was successfully saved", message: nil, preferredStyle: .alert)
                    //                    let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                    //                    alertController.addAction(defaultAction)
                    //                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
        
    }
    
    @objc func returnToRecording(sender: UIButton!) {
        print("Returning")
        dismiss(animated: false) { [weak self] in
            self?.avPlayer.pause()
            
        }
        
        //performSegue(withIdentifier: "BackToRecordViewController", sender: nil)
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
        self.isPlaying = false
        
        self.avPlayer.pause()
    }
    
    func addVideoDidEndNotificationObserver() {
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: self.avPlayer.currentItem, queue: .main) { [weak self] _ in
            self?.avPlayer.seek(to: CMTime.zero)
            //(self?.isPlaying)! ? self?.avPlayer.play() : self?.avPlayer.pause()
            self?.avPlayer.play()
            
            
            //self?.returnButton.sendActions(for: .touchUpInside)
            
            
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! RecordingViewController
        engine.assetArray = []
        DispatchQueue.main.async {
            //            vc.transitionButton.isHidden = true
            
        }
        print("segue back established")
        
        //vc.videoURL = sender as! URL
    }
}


//class MyViewController : UIViewController {
//    
//    let MasterView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 200.0, height: 200.0))
//    
//    

//    
//    var timer: CGFloat = 0.0 {
//        didSet {
//            timer = timer.truncatingRemainder(dividingBy: 11.0)
//            timelineView.indicatorPosition = timer / 10.0
//        }
//    }
//    
//    override func viewDidLoad() {
//        
//        MasterView.addSubview(timelineView)
//        MasterView.backgroundColor = .black
//        
//        timelineView.center = CGPoint(x: view.bounds.midX / 2.0, y: view.bounds.midY)
//        DispatchQueue.main.async {
//            //Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) {_ in self.timer += 1.0 }
//        }
//        
//        self.view = MasterView
//    }
//    
//}
