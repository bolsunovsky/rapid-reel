//: A UIKit based Playground for presenting user interface

import UIKit



enum TimelineState {
    case started(offset: CGFloat)
    case changed(offset: CGFloat)
    case ended(offset: CGFloat)
    
}
protocol TimelineDelegate {
    //var state: TimelineState { get set }
    var position: CGFloat { get set }
}


class TimelineView: RoundedRect {
    
    var touchLocation: CGFloat = 0.0
    var delegate: TimelineDelegate? {
        didSet {
            
        }
    }
    
    //set between 0.0 and 1.0
    private var inset = (x: 5.0, y: 5.0)
    
    var indicatorPosition: CGFloat = 0.0 {
        didSet {
            //indicatorPosition = max(0.0, min(1.0, indicatorPosition))
            if indicatorPosition > 1.0 { indicatorPosition = 1.0 }
            if indicatorPosition < 0.0 { indicatorPosition = 0.0 }
            indicator.transform.tx = (self.bounds.width - (indicator.bounds.width + CGFloat((inset.x * 2.0)))) * indicatorPosition
            //            print(indicatorPosition)
            delegate?.position = indicatorPosition
        }
    }
    
    lazy var indicator: RoundedRect = {
        var view = RoundedRect(frame: CGRect(x: inset.x, y: inset.y, width: 8.0, height: Double(self.bounds.height - 10.0)), cornerRadius: 0.5, gradients: [.red, .red])
        //view.center = CGPoint(x: view.center.x - (view.bounds.width), y: view.center.y)
        view.border(lineWidth: 0.3, color: .black)
        view.layer.mask?.borderWidth = 2.0

        
        
        view.isUserInteractionEnabled = false
        
        let gestureRecogniser = UILongPressGestureRecognizer()
        gestureRecogniser.minimumPressDuration = 0
        
        gestureRecogniser.addTarget(self, action: #selector(gestureRecognizer))
        
        addGestureRecognizer(gestureRecogniser)

        
        return view
    }()
    
//    @objc func indicatorPanned(recognizer: UIPanGestureRecognizer) {
//        switch recognizer.state {
//        case .began:
//            print("began")
//
//
//        case .ended: break
////            delegate?.state = .ended(offset: indicatorPosition)
//
//        default:
//            break
//        }
//
//    }
    
    @objc func gestureRecognizer(recognizer: UILongPressGestureRecognizer) {
        
        switch recognizer.state {
        case .began:
            touchLocation = recognizer.location(in: self).x - CGFloat(inset.x * 2)
            
            indicatorPosition = touchLocation / ((self.bounds.width - (indicator.bounds.width + CGFloat((inset.x * 2.0)))))// - 0.04
            
//            delegate?.state = .started(offset: indicatorPosition)
        case .changed:
            touchLocation = recognizer.location(in: self).x - CGFloat(inset.x * 2)
            
            indicatorPosition = touchLocation / ((self.bounds.width - (indicator.bounds.width + CGFloat((inset.x * 2.0)))))// - 0.04
//            delegate?.state = .changed(offset: indicatorPosition)
        case .ended:
            break
//            delegate?.state = .ended(offset: indicatorPosition)
            
            
        default:
            break
        }
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)

    }
    
    
    convenience init(frame: CGRect, cornerRadius: CGFloat, gradients: [UIColor]) {
        self.init(frame: frame)
        self.cornerRadius = cornerRadius
        self.gradientLayerColours = gradients
        
        addSubview(indicator)
        

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")

    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        
        border(lineWidth: 0.3, color: .black)

        
    }
    
}

