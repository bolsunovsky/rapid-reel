
import UIKit

protocol Asset {
    var timestamps: [Double] { get set }
    
}

class TextUnit: UILabel, UIGestureRecognizerDelegate, Asset {
    var timestamps: [Double] = [Double]()
    
    
    //    var uText: String = ""
    //    var uColour: UIColor = .clear
    //    var uFont: UIFont? = UIFont(name: "Helvetica", size: 28)
    
    public var displayFont: UIFont! {
        didSet {
            textLayer.font = displayFont
        }
    }
    
    public var displayColour: UIColor! {
        didSet {
            textLayer.foregroundColor = displayColour.cgColor
        }
    }
    
    public var displayText: String! {
        didSet {
            textLayer.string = displayText
        }
    }
    
    var textLayer: CATextLayer {
        return layer.sublayers?.first as! CATextLayer
    }
    
    var panGestureRecognizer = UIPanGestureRecognizer()
    
    //    override class var layerClass: AnyClass {
    //        return CATextLayer.self
    //    }
    
    override func awakeFromNib() {
        
    }
    
    @objc func fingersPinched(_ pinch: UIPinchGestureRecognizer) {
        
        if let view = pinch.view {
            view.transform = view.transform.scaledBy(x: pinch.scale, y: pinch.scale)
            pinch.scale = 1
        }
    }
    
    
    @objc func fingersRotated(_ recognizer : UIRotationGestureRecognizer) {
        if let view = recognizer.view {
            //view.transform = view.transform.rotated(by: recognizer.rotation)
            recognizer.rotation = 0
        }
    }
    
    @objc private func fingerPanned(_ gestureRecognizer: UIPanGestureRecognizer) {
        switch gestureRecognizer.state {
        case .began: break
            
        case .changed:
            
            let translation = gestureRecognizer.translation(in: self)
            //gestureRecognizer.view!.center.y = gestureRecognizer.view!.center.y + translation.y
            //gestureRecognizer.view!.center.x = gestureRecognizer.view!.center.x + translation.x
            layer.position.y = gestureRecognizer.view!.center.y + translation.y
            layer.position.x = gestureRecognizer.view!.center.x + translation.x
            
            //textLayer.frame = gestureRecognizer.view!.frame
            
            //gestureRecognizer.view?.transform.translatedBy(x: translation.x, y: translation.y)
            
            //            gestureRecognizer.view?.transform.tx += translation.x
            //            gestureRecognizer.view?.transform.ty += translation.y
            
            gestureRecognizer.setTranslation(.zero, in: self)
        case .ended, .cancelled:
            break
            
        default:
            break
        }
    }
    
    
    func gestureRecognizer(_: UIGestureRecognizer,
                           shouldRecognizeSimultaneouslyWith shouldRecognizeSimultaneouslyWithGestureRecognizer:UIGestureRecognizer) -> Bool {
        return false
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupGestureRecognizers()
        setupTextLayers()
        layer.addSublayer(self.textLayer)
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupGestureRecognizers()
        setupTextLayers()
        layer.addSublayer(self.textLayer)
        
    }
    
    func setupTextLayers() {
        
        let textLayer = CATextLayer()
        textLayer.backgroundColor = UIColor.clear.cgColor
        textLayer.contentsScale = UIScreen.main.scale
        textLayer.string = "Text String"
        textLayer.font = UIFont(name: "Helvetica", size: 28)
        //titleLayer.shadowOpacity = 0.5
        textLayer.alignmentMode = CATextLayerAlignmentMode.center
        textLayer.frame = CGRect(x: bounds.minX, y: bounds.minY, width: bounds.width, height: bounds.height)
        //)
        self.layer.addSublayer(textLayer)
    }
    
    
    func setupGestureRecognizers() {
        let panRecognizer = UIPanGestureRecognizer(target: self, action: #selector(TextUnit.fingerPanned(_:)))
        self.panGestureRecognizer = panRecognizer
        self.addGestureRecognizer(panGestureRecognizer)
        panRecognizer.delegate = self
        
        let pinchRecognizer = UIPinchGestureRecognizer(target: self, action: #selector(TextUnit.fingersPinched(_:)))
        self.addGestureRecognizer(pinchRecognizer)
        pinchRecognizer.delegate = self
        
        let rotationRecognizer = UIRotationGestureRecognizer(target: self, action: #selector(TextUnit.fingersRotated(_:)))
        self.addGestureRecognizer(rotationRecognizer)
        rotationRecognizer.delegate = self
        
        
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        //setupGestureRecognizers()
        
        //titleLayer.shadowOpacity = 0.5
        //        alignmentMode = CATextLayerAlignmentMode.center
        
        
        isUserInteractionEnabled = true
        
        //self
    }
}
