//
//  ViewController.swift
//  CameraApp
//
//  Created by Alexander B on 02/04/2019.
//  Copyright © 2019 Alexander B. All rights reserved.
//

import UIKit

class MainScrollView: UIViewController, UIScrollViewDelegate {
  
    public var subviews = [UIView]() {
        didSet {
            for view in view.subviews {
                view.removeFromSuperview()
            }
            for (i, s) in subviews.enumerated() {
                view.addSubview(s)
                view.frame = CGRect(x: view.frame.width * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height)

            }
        }
        
    }

    
    @IBOutlet weak var backgroundColourMaskImage: UIView!
    
    //Fixating the scrollView's offset to -1.0 or 1.0
    static var scrollViewContentOffset: CGFloat = 0.0 {
        didSet {
            scrollViewContentOffset = max(min(scrollViewContentOffset, 1.0), -1.0)
        }
    }
    
    
//    @IBOutlet weak var scrollView: UIScrollView! {
//        didSet {
//            scrollView.delegate = self
//        }
//    }
    
    
    func setupScrollView() {
        
        let scrollView  = UIScrollView(frame: view.frame)
        scrollView.delegate = self
        view = scrollView
        
        
        scrollView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        scrollView.contentSize = CGSize(width: view.frame.width * CGFloat(subviews.count), height: view.frame.height)
        scrollView.contentOffset.x = view.frame.width
        scrollView.isPagingEnabled = true
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.contentSize.height = 1.0
        scrollView.decelerationRate = .fast
        
        //scrollView.contentSize = CGSize(width: view.frame.width * CGFloat(subviews.count), height: view.bounds.height)
        var playbackvc1 = UIView(frame: view.frame)
        var playbackvc2 = UIView(frame: view.frame)
        playbackvc2.backgroundColor  = .red

        subviews = [playbackvc1, playbackvc2]
    }
    
    
    convenience init(views: [UIView]) {
        self.init()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        //backgroundColourMaskImage
        
        setupScrollView()
    }
    
}

enum Dir {
    case right
    case top
    case left
    case down
}
