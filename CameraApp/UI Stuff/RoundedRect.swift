//
//  RoundedRect.swift
//  CameraApp
//
//  Created by Alexander B on 01/05/2019.
//  Copyright © 2019 Alexander B. All rights reserved.
//

import UIKit

class RoundedRect: UIControl {
    
    public var cornerRadius: CGFloat = 0.0 {
        didSet {
            self.gradientLayer.frame = bounds
            self.layer.mask = roundedMask
        }
    }
    
    public var gradientLayerColours: [UIColor] = [UIColor.red, UIColor.purple] {
        didSet { gradientLayer.colors = gradientLayerColours.compactMap { colour in print(colour.cgColor); return colour.cgColor }
            
        }
    }
    
    //border layers
    lazy var borderLayer: CAShapeLayer = {
        let borderLayer = CAShapeLayer()
        borderLayer.path = roundedMask.path
        borderLayer.fillColor = UIColor.clear.cgColor
        borderLayer.strokeColor = UIColor.clear.cgColor
        borderLayer.lineWidth = 1
        borderLayer.frame = bounds
        return borderLayer
    }()
    
    
    public func border(lineWidth: CGFloat, color: UIColor) {
        borderLayer.strokeColor = color.cgColor
        borderLayer.lineWidth = lineWidth
        
    }
    
    lazy var roundedMask: CAShapeLayer = {
        let mask = CAShapeLayer()
        mask.path = UIBezierPath(roundedRect: bounds, cornerRadius: bounds.width * cornerRadius).cgPath
        
        return mask
    }()
    
    
    lazy var gradientLayer: CAGradientLayer = {
        let tempLayer = CAGradientLayer()
        tempLayer.frame = bounds
        tempLayer.colors = gradientLayerColours.compactMap({$0.cgColor})
        tempLayer.startPoint = CGPoint(x: 0, y: 0)
        tempLayer.endPoint = CGPoint(x: 1, y: 1)
        
        return tempLayer
    }()
    
    
    convenience init(frame: CGRect, cornerRadius: CGFloat, gradients: [UIColor]) {
        self.init(frame: frame)
        self.cornerRadius = cornerRadius
        self.gradientLayerColours = gradients
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.addSublayer(gradientLayer)
        self.layer.mask = roundedMask
        self.layer.addSublayer(borderLayer)
    }
}

class Circle: RoundedRect {
    
    convenience init(frame: CGRect, gradients: [UIColor]) {
        self.init(frame: frame)
        self.cornerRadius = 0.5
        self.gradientLayerColours = gradients
    }
    
}

