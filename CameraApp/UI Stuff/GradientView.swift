import UIKit


class GradientView: UIControl {
    
    var gradientColors = [UIColor.orange, UIColor.red] {
        didSet { gradientLayer.colors = gradientColors.compactMap { colour in colour.cgColor } }
    }
    
    var roundedCornerRatio: CGFloat = 0.0 {
        didSet {
            //update mask layer
            gradientLayer.frame = bounds
            
            let cornerPath = UIBezierPath(roundedRect: bounds, cornerRadius: bounds.width / 2 * roundedCornerRatio)
            let maskLayer = CAShapeLayer()
            maskLayer.path = cornerPath.cgPath
            layer.mask = maskLayer
            //may need to be redrawn as well
        }
    }
    
    private var gradientLayer: CAGradientLayer = {
        //setup
        let gradientLayer = CAGradientLayer()
        (gradientLayer.startPoint, gradientLayer.endPoint) = (CGPoint(x: 0.0, y: 0.0), CGPoint(x: 0.0, y: 1.0))
        return gradientLayer
    }()
    
    convenience init() {
        self.init(frame: CGRect.zero)
        //   self.init(frame: CGRect(origin: CGPoint(x: 0.0, y: 0.0), size: CGSize(width: 50.0, height: 50.0)))
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layer.addSublayer(gradientLayer)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}
