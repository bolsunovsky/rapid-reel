//
//  CameraViewController.swift
//  CameraApp
//
//  Created by Alexander B on 06/04/2019.
//  Copyright © 2019 Alexander B. All rights reserved.
//


extension Double {
    var truncatedToString: String {
        return (String(format: "%.2f", self))
    }
}

import UIKit


class RecordingViewController: UIViewController, EngineDelegate {
    
    var isCameraRotated = false
    var recordButtonCenter = CGPoint.zero
    var verticalLagOffset: CGFloat = 0.0
    
    var initialPositionOfTouch = CGPoint.zero

    //TODO:move to engine
    var videoRecordingTime = 0.0 {
        didSet {
            
            if videoRecordingTime == 0.0 {
                self.timerLabel.text = " ●"
                self.timerLabel.textColor = #colorLiteral(red: 0.6650848985, green: 0.7128792405, blue: 0.7711670399, alpha: 0.4055897887)
//                self.timerLaber.font = timerLaber.font.withSize(20)


            }
            self.timerLabel.text = ("● " + self.videoRecordingTime.truncatedToString)
        }
    }
    
    func startedRecording() {

        
//        timerLaber.isHidden = false
        createTimer()
        
        
        UIView.transition(with: timerLabel, duration: 0.3, options: .transitionCrossDissolve, animations: {
            
            self.timerLabel.textColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
//            self.timerLaber.font = self.timerLaber.font.withSize(20.1)
        }, completion: nil)
        
    }
    
    func endedRecording() {
        //        timerLaber.isHidden = true
        destroyTimer()

//        self.timerLaber.font = timerLaber.font.withSize(20)

        UIView.transition(with: timerLabel, duration: 0.3, options: .transitionCrossDissolve, animations: {

            self.timerLabel.textColor = #colorLiteral(red: 0.6650848985, green: 0.7128792405, blue: 0.7711670399, alpha: 0.4055897887)
            self.timerLabel.text = ("● \(self.videoRecordingTime.truncatedToString)")

        }, completion: nil)
        

    }

    var isTestingLayout = false
    
    
    func setZoom(to factor: CGFloat) {
        camera.zoomFactor = factor
    }
    
    var ratio: CGFloat = 0.0
    
    var timer: Timer?
    
    @objc func increaseTimer() {
        
        videoRecordingTime += 0.01
        //timerLaber.text
        
    }
    
    var recordingsArrayObserver: Any?
    let camera = CameraEngine.sharedInstance
    
    lazy var transitionButton: UIButton = {
        let width: CGFloat = 100
        let button = UIButton(type: .system)
        let origin = CGPoint(x: (self.view.bounds.width / 2) - (width / 2), y: (self.view.bounds.height - ((self.view.bounds.height / 10))))
        button.frame = CGRect(origin: origin, size: CGSize(width: width, height: 20))
        
        button.tintColor = .green
        button.setTitle("Preview", for: .normal)
        button.addTarget(self, action: #selector(viewRec), for: UIControl.Event.touchDown)
        
        button.isHidden = false
        return button
    }()
    

    lazy var slidingViewBot: UILabel = {
        var label = UILabel()
        let height: CGFloat = 70.0
        let origin = CGPoint(x: (self.view.bounds.minX), y: (self.view.bounds.maxY + height))
        label.frame = CGRect(origin: origin, size: CGSize(width: self.view.bounds.width, height: height))
        label.text = "Zoom"
        label.textAlignment = .center
        label.textColor = .white
        label.backgroundColor = .green
        label.font = label.font.withSize(30) //label.font = UIFont(name:"fontname", size: 20.0)

        return label
    }()
    
    lazy var timerLabel: UILabel = {
        
        let width: CGFloat = 1800

        let label = UILabel()
        let origin = CGPoint(x: (self.view.bounds.width / 2) - (width / 2) - 1.0, y: (self.view.bounds.height / 20))
        label.frame = CGRect(origin: origin, size: CGSize(width: width, height: 50))
        label.textAlignment = .center
        label.text = "● 0.00"
        label.textColor = #colorLiteral(red: 0.6650848985, green: 0.7128792405, blue: 0.7711670399, alpha: 0.4055897887)
        //label.adjustsFontSizeToFitWidth = true
        //label.sizeToFit()
        label.font = label.font.withSize(20.111111111111) //label.font = UIFont(name:"fontname", size: 20.0)

        return label
        
    }()
    
    lazy var recordButton: GradientView = {
        let button = GradientView()
        //let button = UIButton(frame: CGRect.zero); button.setTitle("Something here", for: UIControl.State.normal)
        let size: CGFloat = 80
        let origin = CGPoint(x: (view!.bounds.width / 2) - (size / 2), y: (view!.bounds.height / 1.8) - (size / 2))
        button.frame = CGRect(origin: origin, size: CGSize(width: size, height: size))
        button.gradientColors = [UIColor.red, UIColor.red]
        button.roundedCornerRatio = 1.0
        
        let panRecognizer = UIPanGestureRecognizer()
        panRecognizer.addTarget(self, action: #selector(fingerPanned))
        button.addGestureRecognizer(panRecognizer)
        
        //        button.addTarget(self, action: #selector(recordButtonDragged(_:_:)), for: UIControl.Event.touchDragInside)
        
        
        button.isHidden = false
        return button
    }()

    lazy var rotateCameraButton: UIButton = {
        let size: CGFloat = 50.0
        let origin = CGPoint(x: view!.bounds.minX + (size / 2.0), y: view!.bounds.minY + 50.0)
        
        let button = UIButton(frame: CGRect(origin: origin, size: CGSize(width: size, height: size)))
        let newColor = UIColor(ciColor: .red)
        button.tintColor = newColor
        button.setTitle("⮐", for: .normal)
        button.addTarget(self, action: #selector(rotateCamera), for: UIControl.Event.touchUpInside)
        return button
    }()
    
    @objc func rotateCamera() {
        camera.rotateCameraInput()
    

    }
    
    @objc func buttonDown(sender:GradientView!) {

        
        camera.start()
        

        let timingParameters = UISpringTimingParameters(dampingRatio: 0.8)
        let animator = UIViewPropertyAnimator(duration: 0.8, timingParameters: timingParameters)
        animator.addAnimations {
            
            
            self.recordButton.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
                        
        }
        
        animator.startAnimation()
        
        print("Recording")
        
    }
    
    
    
    private func liftFingerUp(sender:GradientView!) {
        
        camera.stop {
            
        }
        
        let timingParameters = UISpringTimingParameters(dampingRatio: 0.8)
        let animator = UIViewPropertyAnimator(duration: 0.8, timingParameters: timingParameters)
        animator.addAnimations {
            
            sender.transform = .identity
            sender.alpha = 1.0
            sender.gradientColors = [UIColor.red, UIColor.red]
            
            
            
        }
        
        animator.isInterruptible = true
        animator.startAnimation()
        
        print("Paused")
        
    }
    @objc func buttonUp(sender:GradientView!) {
        liftFingerUp(sender:sender)
    }

    func checkTransitionButton() {
        DispatchQueue.main.async {
            if self.camera.assetArray.count == 0 {
                
                self.transitionButton.isHidden = true
            } else {
                self.transitionButton.isHidden = false
            }
        }

    }
    
    @objc func fingerPinched(_ gestureRecognizer : UIPinchGestureRecognizer) { guard gestureRecognizer.view != nil else { return }
        
        
        if gestureRecognizer.state == .began || gestureRecognizer.state == .changed {

            
            gestureRecognizer.view!.bounds = CGRect(x: view!.bounds.minX, y: view!.bounds.minY, width: gestureRecognizer.view!.bounds.width * gestureRecognizer.scale, height: gestureRecognizer.view!.bounds.height * gestureRecognizer.scale)
            gestureRecognizer.scale = 1.0
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        recordButton.addTarget(self, action: #selector(buttonDown), for: [.touchDown])
        recordButton.addTarget(self, action: #selector(buttonUp), for: [.touchUpInside])
        
            //, .touchCancel, .touchDragExit, .touchUpOutside])
        
        if isTestingLayout {
            let pinchRecognizer = UIPinchGestureRecognizer()
            pinchRecognizer.addTarget(self, action: #selector(fingerPinched))
            view.addGestureRecognizer(pinchRecognizer)
        }

        
        

        view.addSubview(recordButton)
        view.addSubview(transitionButton)
        view.addSubview(rotateCameraButton)
        view.addSubview(timerLabel)
        view.addSubview(slidingViewBot)

        
        recordingsArrayObserver = camera.observe(\CameraEngine.assetArray, options: [.new]) {engine,_ in
            
            self.checkTransitionButton()
        }

        checkTransitionButton()
    }

    


    func createTimer() {
        guard timer == nil else {
            return
        }
        timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(increaseTimer), userInfo: nil, repeats: true)
        //timer?.tolerance = 0.0001
    }
    
    func destroyTimer() {
        guard timer != nil else {
            return
        }

        timer?.invalidate()
        timer = nil
        //timer = nil
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        recordingsArrayObserver = nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        camera.startSession()

     
        
        camera.delegate = self
        
        view.backgroundColor = .black
        //ask for premission
        //set up layer
        guard let previewLayer = camera.videoPreviewLayer else { return }
        previewLayer.frame = view.frame
        view.layer.addSublayer(previewLayer)
        view.layer.masksToBounds = true

    }
    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        if isTestingLayout { ratio = view.bounds.width / 375.0; print(ratio)}

        
//        calculateRatio()
//        recordButton.transform = CGAffineTransform(scaleX: ratio, y: ratio)
        
        slidingViewBot.bounds = CGRect(x: self.view.frame.midX, y: self.view.frame.maxY, width: self.view.frame.width, height: 75.0)

//        recordButton.bounds = CGRect(recordButton.bounds.)
        
        recordButtonCenter = CGPoint(x: view.bounds.midX, y: view.bounds.maxY - 155.5)

        recordButton.center = recordButtonCenter
//        recordButton.bounds = CGRect(x: recordButton.bounds.minX, y: recordButton.bounds.minY, width: recordButton.bounds.width * ratio, height: recordButton.bounds.height * ratio)
        rotateCameraButton.center = CGPoint(x: recordButton.center.x + 100, y: recordButton.center.y)
        timerLabel.center = CGPoint(x: view.bounds.midX, y: view.bounds.minY + 60.0)
        transitionButton.center = CGPoint(x: recordButton.center.x, y: recordButton.center.y + 88.0)
    }

    
    @IBAction func viewRec() {
        self.performSegue(withIdentifier: "toPlaybackSegue", sender: nil)
    }


    
    @IBAction func unwindClearData(_ sender: UIStoryboardSegue) {
        if sender.source is PlaybackViewController {
            print("unwound")
            self.videoRecordingTime = 0.0
            self.setZoom(to: 0.0)
            //self.
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let identifier = segue.identifier {
            if identifier == "toPlaybackSegue" {
                //let vc = segue.destination as! PlaybackViewController

                print("segue established")
                
                //vc.videoURL = sender as! URL
                
            }
        }
    }
    
    
    func calculateRatio() {

        if isTestingLayout {
            ratio = view.bounds.width / 375.0
            print(ratio)
            
        }
    }
}


extension RecordingViewController {
    @objc private func fingerPanned(recognizer: UIPanGestureRecognizer) {
        
        switch recognizer.state {
        case .began:
            
            initialPositionOfTouch = recognizer.location(in: view)
            verticalLagOffset = initialPositionOfTouch.y - recordButtonCenter.y

            break
            
        case .changed:
            //TODO: scan for force unwraps
            let horizontalTranslation = recognizer.translation(in: view).x
            let verticalTranslation = recognizer.translation(in: view).y
            print(verticalTranslation)
//            recognizer.translation(in: )
            
            setZoom(to: -((verticalTranslation + verticalLagOffset) / 50.0))
            print(initialPositionOfTouch.y)
            print(recordButtonCenter.y)
            
//            recordButton.transform.tx = horizontalTranslation
//            recordButton.transform.ty = verticalTranslation
            
            recordButton.center = recognizer.location(in: view)
            //reminder: using offset as opposed to location of recogniser
            
            //placeholder camera zoom code
            let zoomTriggerOffset: CGFloat = -0.0
            let maxVerticalOffset = view.bounds.midY
            

            //placeholder camera switch code
            let cameraChangeThresholdOffset = view.bounds.midX / 2
            let thresholdResetPositionOffset = view.bounds.midX / 3
           print(horizontalTranslation)
            if horizontalTranslation > 100.0 {
                if !isCameraRotated {

                    isCameraRotated = true

                    rotateCamera()
                }
                
            } else {
                isCameraRotated = false
            }
            
            
        case .ended:
            
            liftFingerUp(sender: recordButton)
            let timingParameters = UISpringTimingParameters(dampingRatio: 0.3)
            let animator = UIViewPropertyAnimator(duration: 0, timingParameters: timingParameters)
            animator.addAnimations {
                self.recordButton.transform = .identity
                
            }
            
            setZoom(to: 0.0)
        default:
            break
        }
        
    }

}
